package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int maxSize = 20;
    ArrayList<FridgeItem> itemsInFridge = new ArrayList<>();
    

    @Override
    public int nItemsInFridge() {
        
        
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        
        if (totalSize() > nItemsInFridge()) {
            itemsInFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemsInFridge.contains(item)) {
            itemsInFridge.remove(item);
        }
        else {
            throw new NoSuchElementException(); 
        }
        
    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : itemsInFridge) {
            if (item.hasExpired()){
                expiredItems.add(item);        
            }
        }
        for (FridgeItem nasty : expiredItems) {
            itemsInFridge.remove(nasty);
        }
        return expiredItems;
    }



}
